#include "mem.h"
#include "mem_internals.h"

#include <assert.h>
#include <stdio.h>

const int HEAP_SIZE = 128;

static void test1(){
    printf("Test №1: Start\n\n");
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    assert(heap != NULL);
    heap_term();
    printf("Test №1: Success\n\n");
}


static void test2(){
    printf("Test №2: Start\n\n");
    void *heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(stdout, heap);
    void *block = _malloc(64);
    assert(block != NULL);
    debug_heap(stdout, heap);
    _free(block);
    debug_heap(stdout, heap);
    heap_term();
    printf("Test №2: Success\n\n");
}

static void test3(){
    printf("Test №3: Start\n\n");
    void *heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(stdout, heap);
    void *b1 = _malloc(32);
    void *b2 = _malloc(64);
    assert(b1 != NULL && b2 != NULL);
    debug_heap(stdout, heap);
    _free(b1);
    _free(b2);
    debug_heap(stdout, heap);
    heap_term();
    printf("Test №3: Success\n\n");
}


static void test4() {
    printf("Test №4: Start\n\n");
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    void *b1 = _malloc(64);
    assert(b1 != NULL);
    debug_heap(stdout, heap);
    void *b2 = _malloc(HEAP_SIZE * 4);
    assert(b2 != NULL);
    debug_heap(stdout, heap);
    _free(b1);
    _free(b2);
    debug_heap(stdout, heap);
    heap_term();
    printf("Test №4: Success\n\n");
}

static void test5() {
    printf("Test №5: Start\n\n");
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    void *b1 = _malloc(64);
    assert(b1 != NULL);
    debug_heap(stdout, heap);
    void *b2 = _malloc(HEAP_SIZE * 2);
    assert(b2 != NULL);
    debug_heap(stdout, heap);
    void *b3 = _malloc(HEAP_SIZE * 4);
    assert(b3 != NULL);
    debug_heap(stdout, heap);
    _free(b1);
    _free(b2);
    _free(b3);
    heap_term();
    printf("Test №5: Success\n\n");
}

int main() {
    test1();
    test2();
    test3();
    test4();
    test5();
    printf("Happy new year!\n");
    return 0;
}
